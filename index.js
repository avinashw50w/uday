const express = require('express');
const Joi = require('joi');
const { generateQuestion } = require('./helpers');

const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/subtract', async (req, res, next) => {
    try {
        let query = req.query;
        const schema = Joi.object({
            questions: Joi
                .number()
                .integer()
                .min(1)
                .required(),
            minuend: Joi
                .number()
                .integer()
                .min(1)
                .max(10)
                .required(),
            subtrahend: Joi
                .number()
                .integer()
                .min(1)
                .max(Joi.ref('minuend'))
                .required(),
            borrowing: Joi
                .number()
                .integer()
                .min(0)
                .max(1)
        });
        
        await schema.validateAsync(query);

        questions = parseInt(query.questions);
        let listOfQuest = [];

        for (let i = 0; i < questions; i++) {
            listOfQuest.push(generateQuestion(query));
        }

        return res.json(listOfQuest);

    } catch (err) {
        if (err.isJoi === true) err.status = 422;
        console.log(err);
        next(err);
    }
});

app.listen(PORT, () => console.log(`Server listening on port ${PORT}`));