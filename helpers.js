const { shuffle, random, subtract } = require('lodash');

function getCorrectAnswer(minuend, subtrahend) {
    return subtract(minuend, subtrahend);
}

function getOptions(minuend, subtrahend) {
    const correctAnswer = getCorrectAnswer(minuend, subtrahend);
    let options = [correctAnswer];
    let answerString = String(correctAnswer).split("").map(Number);

    if (answerString.length >= 3) {
        for (let i = 0; i < 3; ++i) {
            let option = [...answerString];
            option[i] = (option[i] + i + 1) % 10;
            options.push(parseInt(option.join("")));
        }
    }
    else {
        for (let i = 0; i < 3; ++i) {
            options.push((correctAnswer + i + 1));
        }
    }

    return shuffle(options);
}

function sanitize({ minuend, subtrahend, borrowing }) {
    minuend = parseInt(minuend);
    subtrahend = parseInt(subtrahend);
    borrowing = parseInt(borrowing);
    return { minuend, subtrahend, borrowing };
}

function generateOperands(minuendLen, subtrahendLen, borrowing) {
    let minuend = [], subtrahend = [];
    let randomIndex = random(0, subtrahendLen - 2 >= 0 ? subtrahendLen - 2 : 0);

    for (let i = 0; i < subtrahendLen - 1; ++i) {
        let x = random(0, 9);
        let y = random(0, 9);
        if (!borrowing) {
            if (x < y) [x, y] = [y, x];
        }
        else {
            if (i == randomIndex && x >= y) {
                if (x == y) x = (x + 1) % 10;
                if (x > y) [x, y] = [y, x];
            } 
        }

        minuend.push(x);
        subtrahend.push(y);
    }
    
    let x = random(1, 9),
        y = random(1, 9);
    
    if (x == y) x = (x + 1) % 9 + 1;
    if (x < y) [x, y] = [y, x];
    
    if (subtrahendLen == 1 && borrowing) {
        if (minuendLen > 1)
            if (x > y) [x, y] = [y, x];
    }

    minuend.push(x);
    subtrahend.push(y);

    for (let i = subtrahendLen; i < minuendLen; ++i) {
        let x = i == minuendLen - 1 ? random(1, 9) : random(0, 9);
        minuend.push(x);
    }

    [minuend, subtrahend] = [parseInt(minuend.reverse().join("")), parseInt(subtrahend.reverse().join(""))];
    return [minuend, subtrahend];
}

function generateQuestion(data) {
    let { minuend, subtrahend, borrowing } = sanitize(data);
    [minuend, subtrahend] = generateOperands(minuend, subtrahend, borrowing);
    const correctAnswer = getCorrectAnswer(minuend, subtrahend);
    const options = getOptions(minuend, subtrahend);

    return {
        minuend,
        subtrahend,
        correctAnswer,
        options
    };
}

module.exports = {
    generateQuestion
}